---
layout: job_family_page
title: "Engineering Fellow, Infrastructure"
---

### Engineering Fellow, Infrastructure

The Infrastructure Fellow embodies all the requirements of less senior roles, such as [Distinguished Engineer](/job-families/engineering/infrastructure/distinguished-engineer/) and [Site Reliability Engineer](/job-families/engineering/infrastructure/site-reliability-engineer).

In addition, the role is closely associated with [Engineering Fellow role in our Development Department](/job-families/engineering/backend-engineer/#engineering-fellow).

1. Drive the technical strategy of our GitLab.com Infrastructure
1. Heavily influence the technical strategy of our open-source application maintained by our Development Department
1. Make skill-gap recommendations for future hiring in Infrastructure and other departments
1. Author technical vision artifacts with >1 year time horizon
1. Assist teams throughout Engineering to interpret this vision into actionable backlogs
1. Help Engineering avoid the architecture ["ivory tower"](https://en.wikipedia.org/wiki/Ivory_tower)
1. Spend time with customers to learn their needs

#### Job Grade 

The Engineering Fellow, Infrastructure is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).
